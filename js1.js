console.log('js1 run ' + new Date().toLocaleTimeString());

document.addEventListener('DOMContentLoaded', function ready(e) {
    console.log('js1 ready ' + new Date().toLocaleTimeString());
});

window.onload = function() {
    console.log('js1 loaded ' + new Date().toLocaleTimeString());
}

console.log('js1 alert start ' + new Date().toLocaleTimeString());
alert('js1 alert');
console.log('js1 alert end ' + new Date().toLocaleTimeString());

console.log('js1 done ' + new Date().toLocaleTimeString());
