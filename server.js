const http = require('http');
const fs = require('fs');

const getMimeType = (url) => {
    if (url.indexOf('.html') >= 0) {
        return 'text/html';
    }
    if (url.indexOf('.jpg') >= 0) {
        return 'image/jpeg'
    }
    if (url.indexOf('.js') >= 0) {
        return 'text/javascript';
    }
    if (url.indexOf('.css') >= 0) {
        return 'text/css';
    }
    return 'unknown';
}

const server = http.createServer((req, res) => {
    console.log(`${req.method} -- ${req.url}`);
    const fileName = req.url.substr(1);
    fs.readFile(fileName, (err, data) => {
        let interval = 1000;
        if (fileName.indexOf('.jpg') >= 0) {
            interval = 3000;
        }
        setTimeout(() => {
            if (err) {
                res.writeHead(404, { 'Content-Type': 'text/html' });
                return res.end("404 Not Found");
            } else {
                res.writeHead(200, { 'Content-Type': getMimeType(fileName) });
                res.write(data);
            }
            return res.end();
        }, interval);
    });
});

server.on('clientError', (err, socket) => {
    socket.end('HTTP/1.1 400 Bad Request\r\n\r\n');
});

server.listen(8000);
