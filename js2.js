console.log('js2 run ' + new Date().toLocaleTimeString());

document.addEventListener('DOMContentLoaded', function ready(e) {
    console.log('js2 ready ' + new Date().toLocaleTimeString());
});

window.onload = function() {
    console.log('js2 loaded ' + new Date().toLocaleTimeString());
}

console.log('js2 alert start ' + new Date().toLocaleTimeString());
alert('js2 alert');
console.log('js2 alert end ' + new Date().toLocaleTimeString());

console.log('js2 done ' + new Date().toLocaleTimeString());
